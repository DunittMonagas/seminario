# SEGUIMIENTO DE MÚLTIPLES OBJETIVOS EMPLEANDO VISIÓN POR COMPUTADOR

- [Planteamiento del problema](#Planteamiento-del-problema)
- [Objetivo general](#Objetivo-general)
- [Objetivos específicos](#Objetivos-específicos)
- [Planteamiento inicial](#Planteamiento-inicial)

### Planteamiento del problema

La visión por computador es un campo interdisciplinario, tiene por objetivo dotar al computador de un alto nivel de entendimiento de imágenes digitales y videos, tal como lo hace el sistema visual del humano. Implica la extracción automática, análisis y compresión de información útil de una imagen o una secuencia de imágenes. Un área de esta disciplina consiste en el seguimiento de algún objetivo (personas, autos, productos, entre otros) empleando algoritmos. Estos algoritmos deben ser verificados, para corroborar su precision. Este proceso puede ser manual (empleando un ser humano) o automático. Así, en ambos escenarios, se requiere de un conjunto de datos de entrada al algoritmo de seguimiento. Una excelente fuente de dichos datos se encuentra desarrollado por un grupo de universidades, llamado MOTChallenge.

La estandarización por parte de MOTChallenge de un conjunto de métricas para facilitar la comparación de propuestas en el seguimiento de múltiples objetivos fue el inicio de un avance constante que se ha mantenido desde su introducción en 2015. Cientos de trabajos han sido publicado alrededor del mundo como consecuencia del MOTChallenge, y ahora se tiene una mejor idea del estado del arte con respecto a MOT. El análisis exhaustivo permite concluir que resulta poco probable mejorar los resultados actuales con modelos de afinidad pobres que solo consideren apariencia y velocidad. Se necesita trabajar en el perfeccionamiento de los modelos de afinidad, al considerar más características de la secuencia.

###  Objetivo general

* Implementar un framework genérico para el seguimiento de múltiples objetivos, solo peatones.

### Objetivos específicos

* Entrenar una red neuronal siamesa para el modelo de apariencia.
* Construir un mecanismo espacio-temporal para actualizar el modelo de apariencia de los objetivos.
* Crear un arquetipo para la dinámica de los peatones.
* Aceptar la influencia de entre objetivos mediante un prototipo de interacción.
* Ejecutar pruebas a los distintos prototipos para encontrar el framework óptimo

### Planteamiento inicial

En este documento, se expone un proceso similar a [_Tracking The Untrackable: Learning To Track Multiple Cues with Long-Term Dependencies_](https://arxiv.org/pdf/1701.01909.pdf), bajo el exitoso paradigma de seguimiento por detección de un conjunto de observaciones por frame son generados numerosos tracklets a partir del algoritmo k-caminos más cortos donde la distancia entre detecciones funge como criterio de afinidad inicial. Es evidente que los fragmentos resultantes son numerosos, cortos y además de muy baja confiabilidad.

Desde este punto entra en juego el framework, ver figura, compuesto por tres módulos:

![](images/propuestaFramework.jpg)

* __Modelo de apariencia.__ Define la similitud entre dos muestras usando un modelo de apariencia, una red neuronal siamesa (previamente entrenada) recibe dos muestras y calcula una medida de afinidad entre ellas en función de la apariencia, restricciones espacio-temporales y contextuales. Como característica extra, se sugiere introducir un sistema para actualizar el modelo de apariencia de cada objetivo como en [_Online Multi-Object Tracking Using CNN-based Single Object Tracker with Spatial-Temporal Attention Mechanism_](https://arxiv.org/pdf/1708.02843.pdf).

* __Modelo de movimiento.__ Define la dinámica de los objetivos, el modelo de velocidad constante aplicada a tracklets de al menos tres detecciones valida variaciones suaves en la dirección y rapidez del objetivo, de esta forma se evita asociaciones erradas entre detecciones que no sigan una curva realista en la variación de la magnitud de la velocidad, además la dirección limita el espacio de búsqueda de la siguiente detección.

* __Modelo de interacción.__ Al no asumir independencia entre objetivos es posible estudiar cómo afecta la presencia de un objeto a los demás. El modelo de interacción, incorpora la ubicación de los vecinos de una observación en particular de esta manera es factible estudiar no solo el movimiento de un objetivo, sino cómo influyen las detecciones cercanas en el mismo.

Estos tres componentes son claves para obtener un buen desempeño, finalmente la unificación de estos tres criterios queda entonces a cargo de una función. Al aplicar los resultados obtenido a los tracklets iniciales se obtiene el grafo final, una representación fiable de la escena, basta entonces con aplicar el algoritmo para resolver la asociación de datos.

